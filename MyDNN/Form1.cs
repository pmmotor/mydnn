﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MathNet.Numerics.LinearAlgebra;

namespace MyDNN
{
    public partial class Form1 : Form
    {
        MLP m_mlp = new MLP(1,new List<int>(new int[] { 1 }),1,"tanh");
        public Form1()
        {
            InitializeComponent();
            m_mlp.EvLog += new EventHandler<string>(DoLogging);
        }
        private void DoLogging(object sender, string msg)
        {
            m_tbLog.Text += msg + "\n";
            m_tbLog.Refresh();
        }

        private void m_btnExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void m_btnTest_Click(object sender, EventArgs e)
        {
            DoLogging(this, "Starting test...\n");
            /*# Simple test script to check the class
            //# def main():
            # my_mlp = MLP(tot_inputs=6, tot_hidden=64, tot_outputs=3, activation="tanh")  
            # x = np.array([1.0, 1.0, 1.0, 1.0, 1.0, 1.0])
            # target = np.array([-0.75, 0.25, 0.10])
            # for i in range(500):
            # y = my_mlp.forward(x, verbose=False)
            # print("[step " + str(i) + "] y=" + str(my_mlp.y))
            # my_mlp.backward(y, target, verbose=False)
            # my_mlp.train(x, target, learning_rate=0.1)
            # print */
            m_mlp = new MLP(6,new List<int>(new int[]{ 64 }),3,"tanh");
            Vector<double> x = Vector<double>.Build.DenseOfArray(new double[] { 1.0, 1.0, 1.0, 1.0, 1.0, 1.0 });
            Vector<double> target = Vector<double>.Build.DenseOfArray(new double[] { -0.75, 0.25, 0.10 });
            for (int i=0;i<5;i++)
            {
                m_mlp.Forward(x, true);
                DoLogging(this, "Step "+i.ToString()+": y= "+m_mlp.GetOutput.ToVectorString());
                m_mlp.Backward(target, false);
                m_mlp.Train(x,target, 0.1);
            }
        }
    }
}
