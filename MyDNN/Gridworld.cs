﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using MathNet.Numerics.LinearAlgebra;

namespace MyDNN
{
    public class Gridworld
    {
        private int m_worldRow;
        private int m_worldCol;
        private Matrix<float> m_transMatrix;
        private Matrix<float> m_rewardMatrix;
        private Matrix<float> m_stateMatrix;
        private int[,,] m_indexMatrix;
        private int[,] m_debugMatrix;
        private int m_noActions;
        private int[] m_position = new int[2];
        private bool m_debug = false;

        public int WorldSizeX
        { get { return m_worldRow; } }
        public int WorldSizeY
        { get { return m_worldCol; } }
        public bool Debug
        {
            get { return m_debug; }
            set { m_debug = value; }
        }
        public struct Status
        {
            public int[] position;
            public float reward;
            public bool done;
            public void status(int[] pos,float rew, bool don)
            {
                position = pos;
                reward = rew;
                done = don;
            }
            public void status()
            {
                position = new int[] { 0, 0 };
                reward = 0;
                done = false;
            }
        };

//        Random m_rd = new Random(DateTime.Now.Second);
        Random m_rd = new Random(DateTime.Now.Millisecond);

        public event EventHandler<string> EvLog;
        public Gridworld(int xSize, int ySize, int actionSize)
        {
            m_worldRow = xSize;
            m_worldCol = ySize;
            m_noActions = actionSize;
            m_position[0] = m_rd.Next(0, xSize);
            m_position[1] = m_rd.Next(0, ySize);
            m_transMatrix = Matrix<float>.Build.Dense(actionSize, actionSize, 1.0f / actionSize);
            m_rewardMatrix = Matrix<float>.Build.Dense(xSize, ySize, 0.0f);
            m_stateMatrix = Matrix<float>.Build.Dense(xSize, ySize, 1);
            m_indexMatrix = new int[xSize, ySize, 2];
            m_debugMatrix = new int[xSize, ySize];
        }
        /// <summary> Checks the dimensions of a matrix against the world size </summary>
        private bool checkMatrixSize(Matrix<float> m, bool transMatrix = false)
        {
            int x = transMatrix ? m_noActions : m_worldRow;
            int y = transMatrix ? m_noActions : m_worldCol;
            
            if ((m.ColumnCount != x) || (m.RowCount != y)) return false;
            else return true;
        }
        public bool SetTransitionMatrix(Matrix<float> m)
        {
            bool flag = true;
            if (checkMatrixSize(m, true))
            {
                m_transMatrix = m;
            }
            else
            {
                EvLog(this, "Wrong size of transition matrix!");
                flag = false;
            }
            return flag;
        }
        public bool SetRewardMatrix(Matrix<float> m)
        {
            bool flag = true;
            if (checkMatrixSize(m))
            {
                m_rewardMatrix = m;
            }
            else
            {
                EvLog(this, "Wrong size of rewards matrix!");
                flag = false;
            }
            return flag;
        }
        public bool SetStateMatrix(Matrix<float> m)
        {
            bool flag = true;
            if (checkMatrixSize(m))
            {
                m_stateMatrix = m;
            }
            else
            {
                EvLog(this, "Wrong size of state matrix!");
                flag = false;
            }
            return flag;
        }
        /// <summary> Matrix of indexes, i.e coordinates of points in world </summary>
        public bool SetIndexMatrix(int[,,] m)
        {
            bool flag = true;
            if ((m.GetLength(1) != m_worldRow) && (m.GetLength(2) != m_worldCol) && (m.GetLength(3)!=2))
            {
                EvLog(this, "Wrong size of index matrix!");
                flag = false;
            }
            else
            {
                m_indexMatrix = m;
            }
            return flag;
        }
        public void SetPosition(int x, int y)
        {
            m_position[0] = x;
            m_position[1] = y;
        }
        public int[] Reset(bool exploring_starts = false)
        {
            int row;
            int col;
            if (exploring_starts)
            {
                while (true)
                {
                    row = m_rd.Next(0, m_worldRow);
                    col = m_rd.Next(0, m_worldCol);
                    if (m_stateMatrix[row, col] == 0) break;
                }
            }
            else
            {
                row = m_worldRow-1;
                col = 0;
            }
            // reset debugMatrix
            for (int i = 0; i < m_worldRow; i++)
            {
                for (int j = 0; j < m_worldCol; j++)
                {
                    m_debugMatrix[i, j] = 0;
                }
            }
            m_debugMatrix[row, col] = 1;
            return new int[] {m_indexMatrix[row, col, 0],m_indexMatrix[row, col, 1]};
        }
        /// <summary> Does one step in the world according to the given action (force=true) else
        /// according to the most probable action from the transition matrix) </summary>
        public Status Step(int action, bool force = false)
        {
            Status s = new Status();
            int newAction;
            int[] pos = new int[2];
            if(action>=m_noActions)
            {
                EvLog(this, "Unknown action!");
            }
            else
            {   // selection of new action
                if (force) newAction = action;
                else       newAction = Choice(m_noActions, m_transMatrix.Column(action).ToArray());
                // Generating a new position based on the current position and action
                if (newAction == 0)     pos = new int[] { m_position[0] - 1, m_position[1] }; //#UP
                else if(newAction == 1) pos = new int[] { m_position[0], m_position[1] + 1 }; //#RIGHT
                else if(newAction == 2) pos = new int[] { m_position[0] + 1, m_position[1] }; //#DOWN
                else if(newAction == 3) pos = new int[] { m_position[0], m_position[1] - 1 }; //#LEFT
                else EvLog(this, "Unknown action!");

                // Check if the new position is a valid position
                if ((pos[0] >= 0) && (pos[0] < m_worldRow)) 
                {
                    if ((pos[1] >= 0) && (pos[1]< m_worldCol))
                    {
                        if (m_stateMatrix[pos[0], pos[1]] != -1)
                        {
                            m_position = pos;
                            if (m_debug) m_debugMatrix[pos[0], pos[1]] += 1;
                        }
                    }
                }
            }
            s.position = new int[] { m_indexMatrix[m_position[0],m_position[1],0], m_indexMatrix[m_position[0],m_position[1],1]};
            s.reward = m_rewardMatrix[m_position[0], m_position[1]];
            s.done = m_stateMatrix[m_position[0], m_position[1]]==1.0f;
            return s;
        }
        /// <summary> Print stateMatrix (and debugMatrix) </summary>
        public string Render(string comment = "")
        {
            string rowString = "stateMatrix:\n";
            for (int x = 0; x < m_worldRow; x++)
            {
                for (int y = 0; y < m_worldCol; y++)
                {
                    if (m_position[0] == x && m_position[1] == y)
                    {
                        rowString += "X";
                    }
                    else
                    {
                        if (m_stateMatrix[x, y] == 0f)
                        { rowString += "- "; }
                        else if (m_stateMatrix[x, y] == -1f)
                        { rowString += "# "; }
                        else if (m_stateMatrix[x, y] == 1f)
                        { rowString += "* "; }
                        else
                        { rowString += "? "; }
                    }
                }
                rowString += "\n";
            }
            rowString += "\n\ndebugMatrix:\n";
            int sum = 0;
            for (int x = 0; x < m_worldRow; x++)
            {
                for (int y = 0; y < m_worldCol; y++)
                {
                    rowString += m_debugMatrix[x, y].ToString();// +" ";
                    sum += m_debugMatrix[x, y];
                }
                rowString += "\n";
            }
            rowString += "Sum(debugMatrix) = " + sum.ToString() + "\n";
            rowString += comment;
            return rowString;
        }

        /// <summary> -------------------------------------- </summary>
        /// <summary> Utility to emulate nuphy.random.choice </summary>
        public static readonly ThreadLocal<Random> _random = new ThreadLocal<Random>(() => new Random(DateTime.Now.Millisecond));
        public static IEnumerable<T> Choice<T>(IList<T> sequence, int size, float[] distribution)
        {
            double sum = 0;
            // first change shape of your distribution probablity array
            // we need it to be cumulative, that is:
            // if you have [0.1, 0.2, 0.3, 0.4] 
            // we need     [0.1, 0.3, 0.6, 1  ] instead
            var cumulative = distribution.Select(c => {
                var result = c + sum;
                sum += c;
                return result;
            }).ToList();
            for (int i = 0; i < size; i++)
            {
                // now generate random float. It will always be in range from 0 to 1
                var r = (float)_random.Value.NextDouble();
                // now find first index in our cumulative array that is greater or equal generated random value
                var idx = cumulative.BinarySearch(r);
                // if exact match is not found, List.BinarySearch will return index of the first items greater than passed value, but in specific form (negative)
                // we need to apply ~ to this negative value to get real index
                if (idx < 0)
                    idx = ~idx;
                if (idx > cumulative.Count - 1)
                    idx = cumulative.Count - 1; // very rare case when probabilities do not sum to 1 because of precision issues (so sum is 0.999943 and so on)
                                                // return item at given index
                yield return sequence[idx];
            }
        }
        /// <summary> Single number returned </summary>
        public static T Choice<T>(IList<T> sequence, float[] distribution)
        {
            return Choice(sequence, 1, distribution).First();
        }
        /// <summary> Single number as input instead of range and single number returned </summary>
        public static int Choice(int upTo, float[] distribution)
        {
            return Choice(Enumerable.Range(0, upTo).ToArray(), distribution);
        }
    }
}
