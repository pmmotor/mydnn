﻿using Microsoft.Xna.Framework;

namespace MyDNN
{
    public class MultipleControls_a_Test : MapHost
    {
        protected override void Initialize()
        {
            base.Initialize();

            InitializeMap("a");
        }

        protected override void Update(GameTime gameTime)
        {
            base.Update(gameTime);
        }

        protected override void Draw()
        {
            base.Draw();

            DrawMap();
        }
    }
}
