﻿using System.Collections.Generic;
using System;
using System.Diagnostics;

namespace MyDNN
{
    public class Manager
    {
        NeuralNetwork net;
        int[] m_layers;
        string[] m_activations;
        float[][] m_input;
        float[][] m_output;
        float m_costMax = 0.1f;

        public string Init(int[] layers, string[] activations, float[][] input, float[][] output, float costMax)
        {
            // consistency checks
            if (activations.Length + 1 != layers.Length)
            {
                return "Number of activation functions != Number of hidden layers + 1!";
            }
            else if (input[0].Length != layers[0])
            {
                return "Number of NN-inputs, " + layers[0] + ", does not match with lenght of input data vectors" + input[0].Length;
            }
            else if (output[0].Length != layers[layers.Length - 1])
            {
                return "Number of NN-output, " + layers[layers.Length - 1] + ", does not match with lenght of output data vectors" + output[0].Length;
            }
            else
            {
                m_layers = layers;
                m_activations = activations;
                m_input = input;
                m_output = output;
                m_costMax = costMax;
                return "OK";
            }
        }
        public float Train()
        {
            this.net = new NeuralNetwork(m_layers, m_activations);

            int counter = 0;
            Random rnd = new Random(DateTime.Now.Millisecond);
            while (net.Cost > m_costMax)
            {
                counter++;
                int i = rnd.Next(0, m_input.Length);
                //            Debug.WriteLine("Using index " + i);
                net.BackPropagate(m_input[i], m_output[i]);
            }
            Debug.WriteLine("Cost: " + net.Cost);
            Debug.WriteLine("Number of iterations: " + counter);
            return net.Cost;
        }
        public void Test(float[][] testVec)
        {
            string ri = "";
            string ro = "";
            foreach (float[] vec in testVec)
            {
                ri = FormatVec(vec);
                ro = FormatVec(net.FeedForward(vec));
                Debug.WriteLine("Input: " + ri + " Output: " + ro);
            }
        }
        private string FormatVec(float[] vec)
        {
            string r = "";
            foreach (float f in vec)
            {
                r += f.ToString("F3") + ",";
            }
            r = r.Remove(r.Length - 1, 1);
            return r;
        }
    }
}
