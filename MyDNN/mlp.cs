﻿using System;
using System.Collections.Generic;
using MathNet.Numerics.LinearAlgebra;

namespace MyDNN
{
    public class MLP
    {
        MatrixBuilder<double> M = Matrix<double>.Build;
        VectorBuilder<double> V = Vector<double>.Build;

        public event EventHandler<string> EvLog;

        private Vector<double> m_x = Vector<double>.Build.Dense(1);       //input vector
        private List<Vector<double>> m_h = new List<Vector<double>>();    // hidden output vectors
        private List<Matrix<double>> m_W = new List<Matrix<double>>();    // weights matrices
        private List<Matrix<double>> m_dW = new List<Matrix<double>>();   // delta weights matrices
        private List<Vector<double>> m_z = new List<Vector<double>>();//.Build.Dense(m_noHidden[0]);
        private List<Vector<double>> m_dz = new List<Vector<double>>();//.Build.Dense(m_y.Count);
        private Vector<double> m_y = Vector<double>.Build.Dense(1);       // output vector
        private List<int> m_noHidden = new List<int>();
        private bool m_tanh = false;
        private bool m_sigm = false;
        public MLP(int NoInputs,List<int> NoHidden, int NoOutput, string actFunc)
        {
            m_noHidden = NoHidden;
            // build vectors and matrices
            m_x = V.Random(NoInputs+1);         // add bias       
            m_W = new List<Matrix<double>>();
            for (int i=0;i< NoHidden.Count;i++)
            {
                if (i == 0)
                {
                    m_W.Add(M.Random(NoInputs + 1, NoHidden[i]));              //first hidden layer: weights from input + 1 bias
                    m_dW.Add(M.Random(NoInputs + 1, NoHidden[i]));             //first delta hidden layer: weights from input + 1 bias
                }
                else
                {
                    m_W.Add(M.Random(NoHidden[i - 1] + 1, NoHidden[i]));      //other hidden layers + 1 bias
                    m_dW.Add(M.Random(NoHidden[i - 1] + 1, NoHidden[i]));     //delta hidden layers + 1 bias
                }
                m_z.Add(V.Dense(NoHidden[i]));                              // weighted sum vectors
                m_dz.Add(V.Dense(NoHidden[i]));                             // delta weighted sum vectors
                m_h.Add(V.Dense(NoHidden[i] + 1));                          // hidden output vectors with bias
            }
            m_W.Add(M.Random(NoHidden[NoHidden.Count - 1]+1, NoOutput)); // last hidden layer: weights to output
            m_y = V.Dense(NoOutput);
            if (actFunc == "tanh") m_tanh = true;
            else if(actFunc == "sigmoid") m_sigm = true;
            else if (EvLog != null) EvLog(this, "Unknown activation function");
        }
        public Vector<double> GetOutput { 
            get { return m_y; }
        }
        public void Forward(Vector<double> inputVec, bool verbose)
        {
            if (inputVec.Count != m_x.Count-1)
            {
                if (EvLog != null)
                {
                    EvLog(this, "Wrong length of input vector!");
                }
            }
            else
            {
                // self.x = np.hstack([x, np.array([1.0])]) #add the bias unit
                // self.z1 = np.dot(self.x, self.W1)
                Vector<double> bias = Vector<double>.Build.Dense(1);
                Vector<double> tmp = Vector<double>.Build.Dense(1);
                inputVec.CopyTo(m_x.SubVector(0, m_x.Count - 1));    // copy input
                m_x[m_x.Count - 1] = 1.0;                            // set bias
                tmp = m_z[0];
                Dot(m_x, m_W[0], ref tmp);
                m_z[0] = tmp;
                Activate(m_z[0]).CopyTo(m_h[0].SubVector(0, m_h[0].Count - 1));
                // self.h = np.hstack([self.h, np.array([1.0])]) #add the bias unit
                // self.z2 = np.dot(self.h, self.W2) #shape [tot_outputs]
                m_h[0][m_h[0].Count - 1] = 1.0;   // set bias
                tmp = m_z[m_noHidden.Count - 1];
                Dot(m_h[0], m_W[1], ref tmp);
                m_z[m_noHidden.Count - 1] = tmp;
                m_y = Activate(m_z[m_noHidden.Count - 1]);
                if (verbose && EvLog != null)
                { 
                    EvLog(this, "z1: " + m_z[0].ToVectorString());
                    EvLog(this, "h: " + m_h[0].ToVectorString());
                    EvLog(this, "z2: " + m_z[m_noHidden.Count - 1].ToVectorString());
                    EvLog(this, "y: " + m_y.ToVectorString());
                    EvLog(this, "W1: " + m_W[0].ToMatrixString());
                    EvLog(this, "W2: " + m_W[1].ToMatrixString());
                }
            }
        }
        private void Dot(Vector<double> a, Matrix<double> b, ref Vector<double> z)
        {
            for (int i = 0; i < b.ColumnCount; i++)
            {
                z[i] = a.DotProduct(b.Column(i));
            }
        }
        private Vector<double> Activate(Vector<double> a)
        {
            if (m_tanh)
            {
                return a.PointwiseTanh();
            }
            if (m_sigm)
            {
                //float k = (float)Math.Exp(x);
                //return k / (1.0f + k)
                return a.PointwiseExp()/(1.0f+a.PointwiseExp());
            }
            // default; crash sooner or later
            return null;
        }
        private Vector<double> Derivate(Vector<double> a)
        {
            if (m_tanh)
            {
                return a.Map(, a);
            }
            if (m_sigm)
            {
                return a.Map(,a); // x * (1 - x)
            }
            // default; crash sooner or later
            return null;
        }
        public void Backward(Vector<double> targetVec, bool verbose)
        {
            //if (y.shape != target.shape): raise ValueError("[ERROR] The size of target is wrong!")
            if (targetVec.Count != m_y.Count)
            {
                if (EvLog != null)
                {
                    EvLog(this, "Wrong length of target vector!");
                }
            }
            else
            {
                //#gathering all the partial derivatives
                //dE_dy = -(target - y) #shape [tot_outputs]
                //if (self.activation == "sigmoid"):
                //    dy_dz2 = self._sigmoid_derivative(self.z2)
                //elif(self.activation == "tanh"):
                //    dy_dz2 = self._tanh_derivative(self.z2)
                Vector<double> dE_dy = -(targetVec - m_y);
                m_dz[m_noHidden.Count - 1] = Derivate(m_z[m_noHidden.Count - 1]);
                //dz2_dW2 = self.h
                //dz2_dh = self.W2
                List<Vector<double>> dz_dW = new List<Vector<double>>(m_noHidden.Count);
                List<Matrix<double>> dz_dh = new List<Matrix<double>>(m_noHidden.Count);
                dz_dW.Add(m_h[m_noHidden.Count - 1]);   // last hidden layer
                dz_dh.Add(m_W[m_noHidden.Count - 1]);
                //if (self.activation == "sigmoid"):
                //    dh_dz1 = self._sigmoid_derivative(self.z1) #shape [tot_hidden]
                //elif(self.activation == "tanh"):
                //    dh_dz1 = self._tanh_derivative(self.z1) #shape [tot_hidden]
                m_dz[0] = Derivate(m_z[0]);
                //dz1_dW1 = self.x
                dz_dW.Add(m_x);   // first hidden layer
                // gathering the gradient vector of W2
                // shape = [tot_outputs] * [tot_outputs] * [tot_hidden]
                //dE_dW2 = np.dot(np.expand_dims(dE_dy * dy_dz2, axis = 1),
                //                np.expand_dims(dz2_dW2, axis = 0)).T
                List<Matrix<double>> dE_dW = new List<Matrix<double>>(m_noHidden.Count);
                //dE_dW.Add()
                //#gathering the gradient of W1
                //dE_dW1 = (dE_dy * dy_dz2) #shape = [tot_outputs] * [tot_outputs]
                //dE_dW1 = np.dot(dE_dW1, dz2_dh.T)[0:-1] * dh_dz1 #bias removed: [tot_outputs] * [tot_hidden+1, tot_outputs]
                //dE_dW1 = np.dot(np.expand_dims(dE_dW1, axis = 1),
                //                np.expand_dims(dz1_dW1, axis = 0)).T #[tot_hidden] * [tot_inputs+1]
                //dE_dW.Add(dE_dy* m_dz[m_noHidden.Count - 1]);
                //dE_dW[0] = Dot()
            }



            //if (verbose): print("dE_dW1: " + str(dE_dW1))
            //if (verbose): print("dE_dW2: " + str(dE_dW2))
            //return dE_dW1, dE_dW2
            if (verbose && EvLog != null)
            {
                EvLog(this, "dE_dW1: " + m_dW[0].ToMatrixString());
                EvLog(this, "dE_dW2: " + m_dW[m_noHidden.Count - 1].ToMatrixString());
            }
        }
            public double Train(Vector<double> inputVec, Vector<double> targetVec, double learningRate)
        {
            //y = self.forward(x)
            Forward(inputVec, true);
            //dE_dW1, dE_dW2 = self.backward(y, target)
            Backward(targetVec,false);
            // update the weights
            //self.W2 = self.W2 - (learning_rate * dE_dW2)
            //self.W1 = self.W1 - (learning_rate * dE_dW1)
            for(int i=0; i<m_W.Count; i++)
            {
                m_W[i] = m_W[i] - learningRate * m_dW[i];
            }
            // estimate the error
            //error = 0.5 * (target - y) * *2
            double error = 0.5 * (targetVec - inputVec) * (targetVec - inputVec);
            return error;
        }
    }
}
