﻿ using System.Collections.Generic;
using System;
using System.IO;
using MathNet.Numerics.Random;

namespace MyDNN
{
    public class NeuralNetwork
    {
        //fundamental 
        private int[] layers;//layers
        private float[][] neurons;//neurons
        private float[][] biases;//biasses
        private float[][][] weights;//weights
        private int[] activations;//layers

        private bool m_successfullyInit;
//        Random m_rs = new Random(DateTime.Now.Second);
        Random m_rs = new Random(DateTime.Now.Millisecond);

        //genetic
        private float fitness = 0;//fitness

        //backprop
        private float learningRate = 0.1f;//learning rate

        public NeuralNetwork(int[] layers, string[] layerActivations)
        {
            m_successfullyInit = true;
            this.layers = new int[layers.Length];
            for (int i = 0; i < layers.Length; i++)
            {
                this.layers[i] = layers[i];
            }
            activations = new int[layers.Length - 1];
            // check activation vector
            if (layerActivations.Length != activations.Length)
            {
                m_successfullyInit = false;
            }
            else
            {
                for (int i = 0; i < layers.Length - 1; i++)
                {
                    string act = layerActivations[i];
                    switch (act)
                    {
                        case "sigmoid":
                            activations[i] = 0;
                            break;
                        case "tanh":
                            activations[i] = 1;
                            break;
                        case "relu":
                            activations[i] = 2;
                            break;
                        case "leakyrelu":
                            activations[i] = 3;
                            break;
                        default:
                            activations[i] = 2;
                            m_successfullyInit = false;
                            break;
                    }
                }
                InitNeurons();
                InitBias();
                InitWeights();
                Cost = 1f;   // just for testing, will be set to 0 in backpropagation
            }
        }
        public bool Success
        { get { return m_successfullyInit; } }
        public float Fitness
        { get { return fitness; } }
        public float LearningRate
        { get; set; }
        public float Cost { get; private set; }

        private void InitNeurons()//create empty storage array for the neurons in the network.
        {
            List<float[]> neuronsList = new List<float[]>();
            for (int i = 0; i < layers.Length; i++)
            {
                neuronsList.Add(new float[layers[i]]);
            }
            neurons = neuronsList.ToArray();
        }
        private void InitBias()//initializes random array for the biases being held within the network.
        {
            List<float[]> biasList = new List<float[]>();
            for (int i = 1; i < layers.Length; i++)
            {
                float[] bias = new float[layers[i]];
                for (int j = 0; j < layers[i]; j++)
                {
                    //                bias[j] = UnityEngine.Random.Range(-0.5f, 0.5f);
                    bias[j] = (float)(m_rs.NextDouble() - 0.5);
                    //bias[j] = 1.0f;
                }
                biasList.Add(bias);
            }
            biases = biasList.ToArray();
        }
        private void InitWeights()//initializes random array for the weights being held in the network.
        {
            List<float[][]> weightsList = new List<float[][]>();
            for (int i = 1; i < layers.Length; i++)
            {
                List<float[]> layerWeightsList = new List<float[]>();
                int neuronsInPreviousLayer = layers[i - 1];
                for (int j = 0; j < layers[i]; j++)
                {
                    float[] neuronWeights = new float[neuronsInPreviousLayer];
                    for (int k = 0; k < neuronsInPreviousLayer; k++)
                    {
                        neuronWeights[k] = (float)((m_rs.NextDouble() - 0.5) / 10);
                    }
                    layerWeightsList.Add(neuronWeights);
                }
                weightsList.Add(layerWeightsList.ToArray());
            }
            weights = weightsList.ToArray();
        }

        public float[] FeedForward(float[] inputs)//feed forward, inputs >==> outputs.
        {
            for (int i = 0; i < inputs.Length; i++)
            {
                neurons[0][i] = inputs[i];
            }
            for (int i = 1; i < layers.Length; i++)
            {
                int layer = i - 1;
                for (int j = 0; j < layers[i]; j++)
                {
                    float value = 0f;
                    for (int k = 0; k < layers[i - 1]; k++)
                    {
                        value += weights[i - 1][j][k] * neurons[i - 1][k];
                    }
                    neurons[i][j] = Activate(value + biases[i - 1][j], layer);
                }
            }
            // return output layer vector
            return neurons[layers.Length - 1];
        }
        //Backpropagation implemtation down until mutation.
        private float Activate(float value, int layer)//all activation functions
        {
            switch (activations[layer])
            {
                case 0:
                    return Sigmoid(value);
                case 1:
                    return Tanh(value);
                case 2:
                    return Relu(value);
                case 3:
                    return Leakyrelu(value);
                default:
                    return Relu(value);
            }
        }
        private float ActivateDer(float value, int layer)//all activation function derivatives
        {
            switch (activations[layer])
            {
                case 0:
                    return SigmoidDer(value);
                case 1:
                    return TanhDer(value);
                case 2:
                    return ReluDer(value);
                case 3:
                    return LeakyreluDer(value);
                default:
                    return ReluDer(value);
            }
        }
        private float Sigmoid(float x)//activation functions and their corrosponding derivatives
        {
            float k = (float)Math.Exp(x);
            return k / (1.0f + k);
        }
        private float Tanh(float x) => (float) Math.Tanh(x);
        private float Relu(float x) => (0 >= x) ? 0 : x;
        private float Leakyrelu(float x) => (0 >= x) ? 0.01f * x : x;
        private float SigmoidDer(float x) => x * (1 - x);
        private float TanhDer(float x) => 1 - (x * x);
        private float ReluDer(float x) => (0 >= x) ? 0 : 1;
        private float LeakyreluDer(float x) => (0 >= x) ? 0.01f : 1;

        public void BackPropagate(float[] inputs, float[] expected)//backpropogation;
        {
            float[] output = FeedForward(inputs); //runs feed forward to ensure neurons are populated correctly

            Cost = 0;
            for (int i = 0; i < output.Length; i++) Cost += (float)Math.Pow(output[i] - expected[i], 2);//calculated cost of network

            float[][] gamma;

            List<float[]> gammaList = new List<float[]>();
            for (int i = 0; i < layers.Length; i++)
            {
                gammaList.Add(new float[layers[i]]);
            }
            gamma = gammaList.ToArray();//gamma initialization

            int layer = layers.Length - 2;
            for (int i = 0; i < output.Length; i++) gamma[layers.Length - 1][i] = (output[i] - expected[i]) * ActivateDer(output[i], layer);//Gamma calculation
            for (int i = 0; i < layers[layers.Length - 1]; i++)//calculates the w' and b' for the last layer in the network
            {
                biases[layers.Length - 2][i] -= gamma[layers.Length - 1][i] * learningRate;
                for (int j = 0; j < layers[layers.Length - 2]; j++)
                {

                    weights[layers.Length - 2][i][j] -= gamma[layers.Length - 1][i] * neurons[layers.Length - 2][j] * learningRate;//*learning 
                }
            }

            for (int i = layers.Length - 2; i > 0; i--)//runs on all hidden layers
            {
                layer = i - 1;
                for (int j = 0; j < layers[i]; j++)//outputs
                {
                    gamma[i][j] = 0;
                    for (int k = 0; k < gamma[i + 1].Length; k++)
                    {
                        gamma[i][j] = gamma[i + 1][k] * weights[i][k][j];
                    }
                    gamma[i][j] *= ActivateDer(neurons[i][j], layer);//calculate gamma
                }
                for (int j = 0; j < layers[i]; j++)//iterate over outputs of layer
                {
                    biases[i - 1][j] -= gamma[i][j] * learningRate;//modify biases of network
                    for (int k = 0; k < layers[i - 1]; k++)//itterate over inputs to layer
                    {
                        weights[i - 1][j][k] -= gamma[i][j] * neurons[i - 1][k] * learningRate;//modify weights of network
                    }
                }
            }
        }

        //Genetic implementations down onwards until save.
        public void Mutate(int high, float val)//used as a simple mutation function for any genetic implementations.
        {
            for (int i = 0; i < biases.Length; i++)
            {
                for (int j = 0; j < biases[i].Length; j++)
                {
                    //                biases[i][j] = (UnityEngine.Random.Range(0f, high) <= 2) ? biases[i][j] += UnityEngine.Random.Range(-val, val) : biases[i][j];
                    biases[i][j] = ((float)(m_rs.NextDouble() * high) <= 2) ? biases[i][j] += (float)(m_rs.NextDouble() * 2 * val - val) : biases[i][j];
                }
            }

            for (int i = 0; i < weights.Length; i++)
            {
                for (int j = 0; j < weights[i].Length; j++)
                {
                    for (int k = 0; k < weights[i][j].Length; k++)
                    {
                        weights[i][j][k] = ((float)(m_rs.NextDouble() * high) <= 2) ? weights[i][j][k] += (float)(m_rs.NextDouble() * 2 * val - val) : weights[i][j][k];
                    }
                }
            }
        }
        public int CompareTo(NeuralNetwork other) //Comparing For Genetic implementations. Used for sorting based on the fitness of the network
        {
            if (other == null) return 1;

            if (fitness > other.fitness)
                return 1;
            else if (fitness < other.fitness)
                return -1;
            else
                return 0;
        }
        public NeuralNetwork Copy(NeuralNetwork nn) //For creating a deep copy, to ensure arrays are serialzed.
        {
            for (int i = 0; i < biases.Length; i++)
            {
                for (int j = 0; j < biases[i].Length; j++)
                {
                    nn.biases[i][j] = biases[i][j];
                }
            }
            for (int i = 0; i < weights.Length; i++)
            {
                for (int j = 0; j < weights[i].Length; j++)
                {
                    for (int k = 0; k < weights[i][j].Length; k++)
                    {
                        nn.weights[i][j][k] = weights[i][j][k];
                    }
                }
            }
            return nn;
        }
        //save and load functions
        public void Load(string path)//this loads the biases and weights from within a file into the neural network.
        {
            TextReader tr = new StreamReader(path);
            int NumberOfLines = (int)new FileInfo(path).Length;
            string[] ListLines = new string[NumberOfLines];
            int index = 1;
            for (int i = 1; i < NumberOfLines; i++)
            {
                ListLines[i] = tr.ReadLine();
            }
            tr.Close();
            if (new FileInfo(path).Length > 0)
            {
                for (int i = 0; i < biases.Length; i++)
                {
                    for (int j = 0; j < biases[i].Length; j++)
                    {
                        biases[i][j] = float.Parse(ListLines[index]);
                        index++;
                    }
                }

                for (int i = 0; i < weights.Length; i++)
                {
                    for (int j = 0; j < weights[i].Length; j++)
                    {
                        for (int k = 0; k < weights[i][j].Length; k++)
                        {
                            weights[i][j][k] = float.Parse(ListLines[index]); ;
                            index++;
                        }
                    }
                }
            }
        }
        public void Save(string path)//this is used for saving the biases and weights within the network to a file.
        {
            File.Create(path).Close();
            StreamWriter writer = new StreamWriter(path, true);

            for (int i = 0; i < biases.Length; i++)
            {
                for (int j = 0; j < biases[i].Length; j++)
                {
                    writer.WriteLine(biases[i][j]);
                }
            }

            for (int i = 0; i < weights.Length; i++)
            {
                for (int j = 0; j < weights[i].Length; j++)
                {
                    for (int k = 0; k < weights[i][j].Length; k++)
                    {
                        writer.WriteLine(weights[i][j][k]);
                    }
                }
            }
            writer.Close();
        }
    }
}

