﻿namespace MyDNN
{
    partial class Form1
    {
        /// <summary>
        /// Erforderliche Designervariable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Verwendete Ressourcen bereinigen.
        /// </summary>
        /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Vom Windows Form-Designer generierter Code

        /// <summary>
        /// Erforderliche Methode für die Designerunterstützung.
        /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
        /// </summary>
        private void InitializeComponent()
        {
            this.m_tbLog = new System.Windows.Forms.TextBox();
            this.m_lb1 = new System.Windows.Forms.Label();
            this.m_btnTest = new System.Windows.Forms.Button();
            this.m_btnExit = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // m_tbLog
            // 
            this.m_tbLog.Font = new System.Drawing.Font("Consolas", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.m_tbLog.Location = new System.Drawing.Point(12, 46);
            this.m_tbLog.Multiline = true;
            this.m_tbLog.Name = "m_tbLog";
            this.m_tbLog.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.m_tbLog.Size = new System.Drawing.Size(426, 392);
            this.m_tbLog.TabIndex = 0;
            // 
            // m_lb1
            // 
            this.m_lb1.AutoSize = true;
            this.m_lb1.Location = new System.Drawing.Point(13, 22);
            this.m_lb1.Name = "m_lb1";
            this.m_lb1.Size = new System.Drawing.Size(58, 13);
            this.m_lb1.TabIndex = 1;
            this.m_lb1.Text = "Log output";
            // 
            // m_btnTest
            // 
            this.m_btnTest.Location = new System.Drawing.Point(713, 46);
            this.m_btnTest.Name = "m_btnTest";
            this.m_btnTest.Size = new System.Drawing.Size(75, 23);
            this.m_btnTest.TabIndex = 2;
            this.m_btnTest.Text = "Test MLP";
            this.m_btnTest.UseVisualStyleBackColor = true;
            this.m_btnTest.Click += new System.EventHandler(this.m_btnTest_Click);
            // 
            // m_btnExit
            // 
            this.m_btnExit.Location = new System.Drawing.Point(713, 415);
            this.m_btnExit.Name = "m_btnExit";
            this.m_btnExit.Size = new System.Drawing.Size(75, 23);
            this.m_btnExit.TabIndex = 3;
            this.m_btnExit.Text = "Exit";
            this.m_btnExit.UseVisualStyleBackColor = true;
            this.m_btnExit.Click += new System.EventHandler(this.m_btnExit_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.m_btnExit);
            this.Controls.Add(this.m_btnTest);
            this.Controls.Add(this.m_lb1);
            this.Controls.Add(this.m_tbLog);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox m_tbLog;
        private System.Windows.Forms.Label m_lb1;
        private System.Windows.Forms.Button m_btnTest;
        private System.Windows.Forms.Button m_btnExit;
    }
}

