﻿using System;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Diagnostics;
using MathNet.Numerics.LinearAlgebra;
using MyDNN;

namespace UnitTests
{
    [TestClass]
    public class TestGridworld
    {
        /// <summary> Dummy logging routine for event from GridWorld </summary>
        private void DoLogging(object sender, string msg)
        {
            Debug.WriteLine(msg + "\n");
        }
        [TestMethod]
        public void TestGWInit()
        {
            Gridworld g = new MyDNN.Gridworld(5, 5, 4);
            Assert.IsNotNull(g);
        }
        [TestMethod]
        public void TestGWRender()
        {
            Gridworld g = new MyDNN.Gridworld(5, 5, 4);
            Assert.IsNotNull(g);
            g.EvLog += new EventHandler<string>(DoLogging);
            Matrix<float> m = Matrix<float>.Build.Dense(2,2,new float[] {1f,2f,3f,4f});
            // must fail since grid world is 5x5
            Assert.IsFalse(g.SetStateMatrix(m));
            g = new MyDNN.Gridworld(3, 3, 1);
            m = Matrix<float>.Build.Dense(3, 3, new float[] { 1f,-1f,0f,0f,0f,0f,0f,0f,9f });
            // must pass since grid world is 4x4
            Assert.IsTrue(g.SetStateMatrix(m));
            // set the position outside the world
            g.SetPosition(7, 2);
            string result = g.Render();
            Debug.WriteLine(result);
            Assert.IsTrue(result.Length > 0);
            Assert.IsTrue(result.Contains("?"));
            Assert.IsTrue(result.Contains("-"));
            Assert.IsTrue(result.Contains("#"));
            Assert.IsTrue(result.Contains("*"));
            Assert.IsFalse(result.Contains("X"));
            // set the position inside the world
            g.SetPosition(2, 2);
            result = g.Render();
            Assert.IsTrue(result.Contains("X"));
        }
        [TestMethod]
        public void TestGWStep()
        {
            Gridworld g = new Gridworld(3, 3, 4);
            int[,,] m = new int[3, 3, 2] { { { 2, 0 }, { 2, 1 }, { 2, 2 } },
                                           { { 1, 0 }, { 1, 1 }, { 1, 2 } },
                                           { { 0, 0 }, { 0, 1 }, { 0, 2 } } };
            Assert.IsTrue(g.SetIndexMatrix(m));
            Matrix<float> s = Matrix<float>.Build.Dense(3, 3, new float[] { 1f, -1f, -1f, 1f, 0f, 0f, -0f, 0f, 0f });
            Assert.IsTrue(g.SetStateMatrix(s));
            Matrix<float> r = Matrix<float>.Build.Dense(3, 3, new float[] {3f, 2f, 1f, 6f, 5f, 4f, 9f, 8f, 7f});
            Assert.IsTrue(g.SetRewardMatrix(r));
            Matrix<float> t = Matrix<float>.Build.Dense(4, 4, new float[] { 0.8f, 0.1f, 0.0f, 0.1f, 
                                                                            0.1f, 0.8f, 0.1f, 0.0f,
                                                                            0.0f, 0.1f, 0.8f, 0.1f,
                                                                            0.1f, 0.0f, 0.1f, 0.8f});
            Assert.IsTrue(g.SetTransitionMatrix(t));
            g.SetPosition(1, 1);
            Gridworld.Status status = g.Step(2,true);    // go down to index 0/1
            Assert.IsTrue(status.position[0] == 0 && status.position[1] == 1);
            Assert.IsTrue(status.reward == 4f);
            Assert.IsTrue(status.done == false);
            status = g.Step(1, true);    // go right to index 0/2
            Assert.IsTrue(status.position[0] == 0 && status.position[1] == 2);
            Assert.IsTrue(status.reward == 7f);
            Assert.IsTrue(status.done == false);
            status = g.Step(0, true);    // go up to index 1/2
            Assert.IsTrue(status.position[0] == 1 && status.position[1] == 2);
            Assert.IsTrue(status.reward == 8f);
            Assert.IsTrue(status.done == false);
            status = g.Step(0, true);    // go up to index 2/2
            Assert.IsTrue(status.position[0] == 2 && status.position[1] == 2);
            Assert.IsTrue(status.reward == 9f);
            Assert.IsTrue(status.done == false);
            status = g.Step(3, true);    // go left to index 2/1
            Assert.IsTrue(status.position[0] == 2 && status.position[1] == 1);
            Assert.IsTrue(status.reward == 6f);
            Assert.IsTrue(status.done == true);
        }
        [TestMethod]
        public void TestGWReset()
        {
            Gridworld g = new Gridworld(3, 3, 1);
            int[,,] m = new int[3, 3, 2] { { { 2, 0 }, { 2, 1 }, { 2, 2 } },
                                           { { 1, 0 }, { 1, 1 }, { 1, 2 } },
                                           { { 0, 0 }, { 0, 1 }, { 0, 2 } } };
            Assert.IsTrue(g.SetIndexMatrix(m));
            Matrix<float> s = Matrix<float>.Build.Dense(3, 3, new float[] { 1f, -1f, -1f, -1f, 0f, 0f, -1f, 1f, 1f });
            Assert.IsTrue(g.SetStateMatrix(s));
            // pos must be index of lower left corner (0/0)
            int[] pos = g.Reset(false);
            Assert.IsTrue(pos[0] == 0);
            Assert.IsTrue(pos[1] == 0);
            // pos must be index of state with value 0
            pos = g.Reset(true);
            Assert.IsTrue(pos[0] == 0 || pos[0] == 1);
            Assert.IsTrue(pos[1] == 1);
        }
        [TestMethod]
        public void TestGWIndexMatrix()
        {
            Gridworld g = new Gridworld(3, 3, 1);
            int[,,] m = new int[3, 3, 2] { { { 0, 0 }, { 0, 1 }, { 0, 2 } },
                                           { { 1, 0 }, { 1, 1 }, { 1, 2 } },
                                           { { 2, 0 }, { 2, 1 }, { 2, 2 } } };
            Assert.IsTrue(g.SetIndexMatrix(m));
        }
        [TestMethod]
        public void TestGWSetStateMatrix()
        {
            Gridworld g = new Gridworld(2, 2, 1);
            Matrix<float> m = Matrix<float>.Build.DenseOfArray(new float[,] { {1f,2f },{3f,4f } });
            Assert.IsTrue(g.SetStateMatrix(m));
        }
        [TestMethod]
        public void TestGWSetRewardMatrix()
        {
            Gridworld g = new Gridworld(2, 2, 1);
            Matrix<float> m = Matrix<float>.Build.DenseOfArray(new float[,] { { 1f, 2f }, { 3f, 4f } });
            Assert.IsTrue(g.SetRewardMatrix(m));
        }
        [TestMethod]
        public void TestGWSetTransitionMatrix()
        {
            Gridworld g = new Gridworld(3, 3, 2);
            Matrix<float> m = Matrix<float>.Build.DenseOfArray(new float[,] { { 1f, 2f }, { 3f, 4f } });
            Assert.IsTrue(g.SetTransitionMatrix(m));
        }
        [TestMethod]
        public void TestGWChoice1()
        {
            int[] result = Gridworld.Choice(Enumerable.Range(0, 5).ToArray(), 3, new float[] { 0.01f, 0.01f, 0.48f, 0.48f, 0.02f }).ToArray();
            Assert.IsTrue(result.Length == 3);
        }
        [TestMethod]
        public void TestGWChoice2()
        {
            int result = Gridworld.Choice(5, new float[] { 0.01f, 0.01f, 0.48f, 0.48f, 0.02f });
            Assert.IsTrue(0<=result && result<5);
        }
    }
}
