﻿using System;
using System.Diagnostics;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MathNet.Numerics.LinearAlgebra;
using MyDNN;
using DSharpDXRastertek.Series2.TutTerr02.System;
using System.Windows.Forms;

namespace UnitTests
{
    [TestClass]
    public class TestDNN
    {
        Manager m = new Manager();
        float[][] m_i = new float[8][];
        float[][] m_o = new float[8][];
        Random m_rd = new Random(DateTime.Now.Millisecond);

        [TestMethod]
        public void TestDNN_Or()
        {
            float target_cost = 0.001f;

            m_i[0] = new float[] { 0, 0, 0 };
            m_i[1] = new float[] { 1, 0, 0 };
            m_i[2] = new float[] { 0, 1, 0 };
            m_i[3] = new float[] { 0, 0, 1 };
            m_i[4] = new float[] { 1, 1, 0 };
            m_i[5] = new float[] { 0, 1, 1 };
            m_i[6] = new float[] { 1, 0, 1 };
            m_i[7] = new float[] { 1, 1, 1 };

            m_o[0] = new float[] { 0, 0, 0 };
            m_o[1] = new float[] { 1, 0, 1 };
            m_o[2] = new float[] { 1, 0, 1 };
            m_o[3] = new float[] { 1, 0, 0 };
            m_o[4] = new float[] { 1, 0.5f, 0 };
            m_o[5] = new float[] { 1, 0.5f, 1 };
            m_o[6] = new float[] { 1, 0.5f, 1 };
            m_o[7] = new float[] { 1, 1, 0 };

            string result = m.Init(new int[] { 3, 5, 3 }, new string[2] { "tanh", "tanh" }, m_i, m_o, target_cost);
            Assert.IsTrue(result == "OK");
            float cost = m.Train();
            m.Test(m_i);
            Assert.IsTrue(cost < target_cost);
        }
        [TestMethod]
        public void TestDNN_Flat()
        {
            float target_cost = 0.001f;

            m_i = new float[1][];
            m_i[0] = new float[] { 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f };
            m_o = new float[1][];
            m_o[0] = new float[] { -0.75f, 0.25f, 0.10f };
            string result = m.Init(new int[] { 6, 64, 3 }, new string[2] { "tanh", "tanh" }, m_i, m_o, target_cost);
            Assert.IsTrue(result == "OK");
            float cost = m.Train();
            m.Test(m_i);
            Assert.IsTrue(cost < target_cost);
        }
        [TestMethod]
        public void TestDNN_Xor_Org()
        {
            Matrix<float> s = Matrix<float>.Build.DenseOfArray(new float[,] { { 1f,0, 0, 0, 0, 1f },
                                                                              { 0, 0, 0, 0, 0, 0 },
                                                                              { 0, 0, 0, 0, 0, 0 },
                                                                              { 0, 0, 0, 0, 0, 0 },
                                                                              { 0, 0, 0, 0, 0, 0 },
                                                                              { 1f,0, 0, 0, 0, 1f }});
            int[,,] i = new int[6, 6, 2] { { { 5, 0 }, { 5, 1 }, { 5, 2 }, { 5, 3 }, { 5, 4 }, { 5, 5 } },
                                           { { 4, 0 }, { 4, 1 }, { 4, 2 }, { 4, 3 }, { 4, 4 }, { 4, 5 } },
                                           { { 3, 0 }, { 3, 1 }, { 3, 2 }, { 3, 3 }, { 3, 4 }, { 3, 5 } },
                                           { { 2, 0 }, { 2, 1 }, { 2, 2 }, { 2, 3 }, { 2, 4 }, { 2, 5 } },
                                           { { 1, 0 }, { 1, 1 }, { 1, 2 }, { 1, 3 }, { 1, 4 }, { 1, 5 } },
                                           { { 0, 0 }, { 0, 1 }, { 0, 2 }, { 0, 3 }, { 0, 4 }, { 0, 5 } } };
        Matrix<float> r = Matrix<float>.Build.DenseOfArray(new float[,]     { { 1f, 0, 0, 0, 0,-1f },
                                                                              { 0 , 0, 0, 0, 0, 0 },
                                                                              { 0 , 0, 0, 0, 0, 0 },
                                                                              { 0 , 0, 0, 0, 0, 0 },
                                                                              { 0 , 0, 0, 0, 0, 0 },
                                                                              { -1f,0, 0, 0, 0, 1f }});
            Matrix<float> t = Matrix<float>.Build.DenseOfArray(new float[,] { { 0.8f ,0.1f, 0, 0.1f },
                                                                              { 0.1f, 0.8f, 0.1f, 0},
                                                                              { 0, 0.1f, 0.8f, 0.1f},
                                                                              { 0.1f, 0, 0.1f, 0.8f }});
            //Matrix<float> t = Matrix<float>.Build.DenseOfArray(new float[,] { { 0.8f ,0.8f, 0.8f, 0.8f },
            //                                                                  { 0.8f, 0.8f, 0.8f, 0.8f},
            //                                                                  { 0.8f, 0.8f, 0.8f, 0.8f},
            //                                                                  { 0.8f, 0.8f, 0.8f, 0.8f }});

            Gridworld env = new Gridworld(6, 6, 4);
            env.Debug = true;
            NeuralNetwork nn = new NeuralNetwork(new int[] { 2, 2, 1 }, new string[2] { "tanh", "tanh" });
            Assert.IsTrue(nn.Success);
            nn.LearningRate = 0.1f;
            Assert.IsTrue(env.SetStateMatrix(s));
            Assert.IsTrue(env.SetIndexMatrix(i));
            Assert.IsTrue(env.SetRewardMatrix(r));
            Assert.IsTrue(env.SetTransitionMatrix(t));
            int tot_epoch = 10001;
            int print_epoch = 1000;
            float gamma = 0.9f;
            float error = 0;
            int step = 0;
            for (int epoch = 1; epoch < tot_epoch; epoch++)
            {   // reset position to 
//                int[] position = env.Reset(true);
                // start in the middle of the world
                int[] position = new int[] { env.WorldSizeX/2, env.WorldSizeY/2};
                Gridworld.Status stat = new Gridworld.Status();
                for (step = 0; step < 100000; step++)
                {
                    //action = np.random.randint(0, 4)
                    //new_observation, reward, done = env.step(action) #move in the world and get the state and reward
                    //my_mlp, error = update(my_mlp, new_observation, reward, learning_rate, gamma, done)
                    //observation = new_observation
                    //if done: break
                    int action = m_rd.Next(0, 4);
                    stat = env.Step(action,false);
                    error = update(ref nn, stat, gamma);
                    position = stat.position;
                    if (stat.done)
                    {
                        env.Render("\nNumber of steps:" + step.ToString() + "\n");
                        break;
                    }
                }
                if (epoch % print_epoch == 0)
                {
                    Debug.WriteLine("Epoch:       " + epoch.ToString());
                    Debug.WriteLine("Total steps: " + step.ToString());
                    Debug.WriteLine("Error:       " + error.ToString("F4"));
//                    displayNN(nn, env, "Epoch: " + epoch.ToString() + ", total steps: " + step.ToString() + ", error: " + error.ToString("F4"));
                }
            }
            displayNN(nn, env, "Final epoch: " + (tot_epoch-1).ToString() + ", total steps: " + step.ToString() + ", error: " + error.ToString("F4"));
        }
        [TestMethod]
        public void TestDNN_Xor()
        {
            Matrix<float> s = Matrix<float>.Build.DenseOfArray(new float[,] { { 1f,0, 0, 0, 0, 1f },
                                                                              { 0, 0, 0, 0, 0, 0 },
                                                                              { 0, 0, 0, 0, 0, 0 },
                                                                              { 0, 0, 0, 0, 0, 0 },
                                                                              { 0, 0, 0, 0, 0, 0 },
                                                                              { 1f,0, 0, 0, 0, 1f }});
            int[,,] i = new int[6, 6, 2] { { { 5, 0 }, { 5, 1 }, { 5, 2 }, { 5, 3 }, { 5, 4 }, { 5, 5 } },
                                           { { 4, 0 }, { 4, 1 }, { 4, 2 }, { 4, 3 }, { 4, 4 }, { 4, 5 } },
                                           { { 3, 0 }, { 3, 1 }, { 3, 2 }, { 3, 3 }, { 3, 4 }, { 3, 5 } },
                                           { { 2, 0 }, { 2, 1 }, { 2, 2 }, { 2, 3 }, { 2, 4 }, { 2, 5 } },
                                           { { 1, 0 }, { 1, 1 }, { 1, 2 }, { 1, 3 }, { 1, 4 }, { 1, 5 } },
                                           { { 0, 0 }, { 0, 1 }, { 0, 2 }, { 0, 3 }, { 0, 4 }, { 0, 5 } } };
            Matrix<float> r = Matrix<float>.Build.DenseOfArray(new float[,]     { { 1f, 0, 0, 0, 0,-1f },
                                                                              { 0 , 0, 0, 0, 0, 0 },
                                                                              { 0 , 0, 0, 0, 0, 0 },
                                                                              { 0 , 0, 0, 0, 0, 0 },
                                                                              { 0 , 0, 0, 0, 0, 0 },
                                                                              { -1f,0, 0, 0, 0, 1f }});
            Matrix<float> t = Matrix<float>.Build.DenseOfArray(new float[,] { { 0.8f ,0.1f, 0, 0.1f },
                                                                              { 0.1f, 0.8f, 0.1f, 0},
                                                                              { 0, 0.1f, 0.8f, 0.1f},
                                                                              { 0.1f, 0, 0.1f, 0.8f }});

            Gridworld env = new Gridworld(6, 6, 4);
            env.Debug = true;
            NeuralNetwork nn = new NeuralNetwork(new int[] { 2, 2, 1 }, new string[2] { "tanh", "tanh" });
            Assert.IsTrue(nn.Success);
            nn.LearningRate = 0.1f;
            Assert.IsTrue(env.SetStateMatrix(s));
            Assert.IsTrue(env.SetIndexMatrix(i));
            Assert.IsTrue(env.SetRewardMatrix(r));
            Assert.IsTrue(env.SetTransitionMatrix(t));
            Gridworld.Status stat = new Gridworld.Status();
            float error=1f;
            int counter = 0;
            stat.done = false;
            while (error>0.000001)
            {
                int x = m_rd.Next(0, 4);
                int y = m_rd.Next(0, 4);
                stat.position = new int[] { x, y };
                stat.reward = r[x, y];
                error = update(ref nn, stat, 0.9f);
                counter++;
            }
//            MessageBox.Show(env.Render(),"DebugMatrix");
            displayNN(nn, env, "With " + counter.ToString() + " steps:");
        }
        /// <summary> Trains the network with the given vector stat.position and returns the error </summary>
        private float update(ref NeuralNetwork nn, Gridworld.Status s, float gamma)
        {
            float[] input = new float[] { (float)s.position[0], (float)s.position[1] };
            float[] expected;
            if (s.done)
            {
                expected = new float[] {s.reward };
            }
            else
            {
                expected = new float[] { s.reward + gamma*nn.FeedForward(input)[0] };
            }
            nn.BackPropagate(input, expected);
            return nn.Cost;
        }
        /// <summary> Prints the response of the network for each point in the gridworld</summary>
        private void displayNN(NeuralNetwork n, Gridworld g, string title)
        {
            Matrix<float> result = Matrix<float>.Build.Dense(g.WorldSizeX, g.WorldSizeY);
            for (int i = 0; i < g.WorldSizeX; i++)
            { 
                for(int j=0;j<g.WorldSizeY;j++)
                {
                    result[i, j] = n.FeedForward(new float[] { i, j })[0];
                }
            }
            MessageBox.Show(g.Render(), "Gridworld ouput");
            MessageBox.Show(result.ToString(), "NN ouput");
            DSystem.RenderMatrix(title, 800, 600, result.ToArray(), 0.5f, new float[] { 16, 2, 3 }, new float[] { -5, 269, 0 },true);
            for (int i = 0; i < g.WorldSizeX; i++)
            {
                string row = "Row "+i.ToString()+": ";
                for (int j = 0; j < g.WorldSizeY; j++)
                {
                    row += result[i, j].ToString("F4") + " ";
                }
                Debug.WriteLine(row);
            }
        }
        [TestMethod]
        public void TestRenderForm()
        {
            DSystem.StartRenderForm("TestRenderForm", 800, 400, true, false, 0, 12f);//, new float[] { 40, 24, 10 }, new float[] { 40, 255, 0 });
        }
        [TestMethod]
        public void TestRenderFile()
        {
            string file = "testSetup.txt";
            file = "setupS2TutTerr08.txt";
//            file = "setup.txt";
            DSystem.RenderFile("TestRenderFile", 1000, 600, file, true, false, 0, 30f, new float[] { 55, 250, 30 }, new float[] { 47, 36, 0 });
        }
        [TestMethod]
        public void TestRenderFileSolid()
        {
            string file = "heightmap.bmp";
            DSystem.RenderFile("TestRenderFile", 1000, 600, file, true, false, 0, 30f, new float[] { 55, 250, 30 }, new float[] { 47, 36, 0 },true);
        }
        [TestMethod]
        public void TestRenderMatrixGrid()
        {
            int sizeX = 100;
            int sizeY = 100;
            float[,] data = new float[sizeX, sizeY];
            for (int i = 0; i < sizeX; i++)
            {
                for (int j = 0; j < sizeY; j++)
                {
                    data[i, j] = 10f *(float)Math.Sin((i + 1) / (float)sizeX * 2 * Math.PI) * (float)Math.Cos((i + 1) / (float)sizeX * Math.PI);
                }
            }
            DSystem.RenderMatrix("TestRenderMatrix", 1200, 600, data, 0.5f,new float[] {66,23,-55 }, new float[] {16,346,0 });
        }
        [TestMethod]
        public void TestRenderMatrixSolid()
        {
            int sizeX = 100;
            int sizeY = 100;
            float[,] data = new float[sizeX, sizeY];
            for (int i = 0; i < sizeX; i++)
            {
                for (int j = 0; j < sizeY; j++)
                {
                    data[i, j] = 10f * (float)Math.Sin((i + 1) / (float)sizeX * 2 * Math.PI) * (float)Math.Cos((i + 1) / (float)sizeX * Math.PI);
                }
            }
            DSystem.RenderMatrix("TestRenderMatrix", 1200, 600, data, 0.4f, new float[] { 66, 23, -55 }, new float[] { 16, 346, 0 },true);
        }
        [TestMethod]
        public void TestDNNNeuralNetwork()
        {
            NeuralNetwork nn = new NeuralNetwork(new int[] { 3, 5, 3 }, new string[2] { "tanh", "tanh" });
            Assert.IsTrue(nn.Success);
            nn = new NeuralNetwork(new int[] { 3, 5, 3 }, new string[1] { "tanh" });
            Assert.IsFalse(nn.Success);
            nn = new NeuralNetwork(new int[] { 3, 5, 3 }, new string[2] { "tanh", "fail" });
            Assert.IsFalse(nn.Success);
        }
        [TestMethod]
        public void TestDNNFeedforward()
        {
            float[] input    = new float[] { 0.5f, 0.8f, -0.5f };
            float[] expected = new float[] { 0.1f, 1.0f };
            float[] output = new float[2];
            NeuralNetwork nn = new NeuralNetwork(new int[] { 3, 5, 2 }, new string[2] { "tanh", "tanh" });
            Assert.IsTrue(nn.Success);
            for(int i =0;i<1000;i++) nn.BackPropagate(input, expected);
            output = nn.FeedForward(input);
            Assert.IsTrue(output[0] > 0);
            Assert.IsTrue(output[1] > 0);
        }
        [TestMethod]
        public void TestDNNBackPropagate()
        {
            float[] input = new float[] { 0.5f, 0.8f, -0.5f };
            float[] expected = new float[] { 0.1f, 1.0f };
            float[] output = new float[2];
            NeuralNetwork nn = new NeuralNetwork(new int[] { 3, 5, 2 }, new string[2] { "tanh", "tanh" });
            Assert.IsTrue(nn.Success);
            Assert.IsTrue(nn.Cost == 1f);
            nn.BackPropagate(input, expected);
            Assert.IsFalse(nn.Cost == 1f);
        }
    }
}
