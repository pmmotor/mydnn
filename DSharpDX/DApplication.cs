﻿using DSharpDXRastertek.Series2.TutTerr02.Graphics;
using DSharpDXRastertek.Series2.TutTerr02.Graphics.Input;
using DSharpDXRastertek.Series2.TutTerr02.Graphics.Shaders;
using DSharpDXRastertek.Series2.TutTerr02.Graphics.Models;
using DSharpDXRastertek.Series2.TutTerr02.Graphics.Camera;
using DSharpDXRastertek.Series2.TutTerr02.Graphics.Data;
using SharpDX;
using System;

namespace DSharpDXRastertek.Series2.TutTerr02.System
{
    public class DApplication
    {
        // Properties
        public DInput Input { get; private set; }
        private DDX11 D3D { get; set; }
        public DTimer Timer { get; set; }
        public DFPS FPS { get; set; }
        public DShaderManager ShaderManager { get; set; }
        public DTerrainShader DTerrainShader { get; set; }
        public DZone Zone { get; set; }
        public DCamera Camera { get; set; }
        public DPosition Position { get; set; }
        public DLight Light { get; set; }
        public DTerrain1 Terrain { get; set; }

        private bool m_solid = false;
        public bool Initialize(DSystemConfiguration configuration, IntPtr windowHandle, string title, string file = null, float[,] data = null, float scale=12f, float[] pos=null, float[] rot=null, bool solid = false)
        {
            m_solid = solid;
            // Create the input object.  The input object will be used to handle reading the keyboard and mouse input from the user.
            Input = new DInput();
            // Initialize the input object.
            if (!Input.Initialize(configuration, windowHandle))
                return false;

            // Create the Direct3D object.
            D3D = new DDX11();
            // Initialize the Direct3D object.
            if (!D3D.Initialize(configuration, windowHandle))
                return false;

            Terrain = new DTerrain1();
            if(file!=null) Terrain.Initialize(D3D.Device, file, scale, "grass.bmp", "ice01.bmp", "rock.bmp");
            else Terrain.Initialize(D3D.Device, data, scale, "grass.bmp", "ice01.bmp", "rock.bmp");
            // Create the shader manager object.
            ShaderManager = new DShaderManager();
            // Initialize the shader manager object.
            if (!ShaderManager.Initilize(D3D, windowHandle))
                return false;

            // Create the camera object
            Camera = new DCamera();

            // Initialize a base view matrix with the camera for 2D user interface rendering.
            Camera.SetPosition(0.0f, 0.0f, -1.0f);
            Camera.Render();
            Matrix baseViewMatrix = Camera.ViewMatrix;

            // Set the initial position of the camera. (Since the ViewMatrix is already created from a base position.)
            Camera.SetPosition(150.0f, 2.0f, 220.0f);
            Camera.SetRotation(0, 180.0f, 0);

            // Create the shader  object.
            DTerrainShader =  new DTerrainShader();
            // Initialize the shader manager object.
            if (!DTerrainShader.Initialize(D3D.Device, windowHandle))
                return false;

            // Create and initialize Timer.
            Timer = new DTimer();
            if (!Timer.Initialize())
                return false;

            // Create the fps object.
            FPS = new DFPS();
            FPS.Initialize();

            // Create and Initialize the Zone object.
            if (!solid)
            {
                Zone = new DZone();
                if (!Zone.Initialize(D3D, windowHandle, configuration, title, file, data, scale, pos, rot))
                    return false;
            }

            // Create the position object. 
            Position = new DPosition();
            // Set the initial position and rotation of the viewer.28.0f, 5.0f, -10.0f
            if (pos != null && rot != null && pos.Length == rot.Length && pos.Length == 3)
            {
                Position.SetPosition(pos[0], pos[1], pos[2]);
                Position.SetRotation(rot[0], rot[1], rot[2]);
            }
            else
            {
                Position.SetPosition(128.0f, 10.0f, -10.0f);
                Position.SetRotation(0.0f, 300.0f, 0.0f);
            }

            // Create the light object.
            Light = new DLight();

            // Initialize the light object.
            Light.SetAmbientColor(0.05f, 0.05f, 0.05f, 1.0f);
            Light.SetDiffuseColor(1.0f, 1.0f, 1.0f, 1.0f);
            Light.Direction = new Vector3(-0.5f, -1.0f, 0.0f);
            return true;
        }
        public void Shutdown()
        {
            // Release the zone object.
            Zone?.ShutDown();
            Zone = null;
            // Release the fps object.
            FPS = null;
            // Release the timer object.
            Timer = null;
            // Release the shader manager object.
            ShaderManager?.ShutDown();
            ShaderManager = null;
            // Release the Direct3D object.
            D3D?.ShutDown();
            D3D = null;
            // Release the input object.
            Input?.Shutdown();
            Input = null;
        }
        public bool Frame()
        {
            // Update the system stats.
            FPS.Frame();

            // Do the zone frame processing.
            if (m_solid)
            {
                if (!HandleInput(Timer.FrameTime))
                    return false;
                if (!RenderGraphics())
                    return false;
            }
            else
            {
                if (!Zone.Frame(D3D, Input, ShaderManager, Timer.FrameTime, FPS.FPS))
                    return false;
            }
            return true;
        }
        private bool HandleInput(float frameTime)
        {
            // Set the frame time for calculating the updated position.
            Position.SetFrameTime(frameTime);

            // Handle the input
            bool keydown = Input.IsLeftArrowPressed();
            Position.TurnLeft(keydown);
            keydown = Input.IsRightArrowPressed();
            Position.TurnRight(keydown);
            keydown = Input.IsUpArrowPressed();
            Position.MoveForward(keydown);
            keydown = Input.IsDownArrowPressed();
            Position.MoveBackward(keydown);
            keydown = Input.IsPageUpPressed();
            Position.LookUpward(keydown);
            keydown = Input.IsPageDownPressed();
            Position.LookDownward(keydown);
            keydown = Input.IsAPressed();
            Position.MoveUpward(keydown);
            keydown = Input.IsZPressed();
            Position.MoveDownward(keydown);

            // Set the position and rOTATION of the camera.
            Camera.SetPosition(Position.PositionX, Position.PositionY, Position.PositionZ);
            Camera.SetRotation(Position.RotationX, Position.RotationY, Position.RotationZ);

            // Update the position values in the text object.
//            Text.SetCameraPosition(Position.PositionX, Position.PositionY, Position.PositionZ, D3D.DeviceContext);

            // Update the rotation values in the text object.
//            Text.SetCameraRotation(Position.RotationX, Position.RotationY, Position.RotationZ, D3D.DeviceContext);
            return true;
        }
        private bool RenderGraphics()
        {
            // Clear the scene.
            D3D.BeginScene(0.0f, 0.0f, 0.0f, 1.0f);

            // Generate the view matrix based on the camera's position.
            Camera.Render();

            // Get the world, view, projection, and ortho matrices from the camera and Direct3D objects.
            Matrix worldMatrix = D3D.WorldMatrix;
            Matrix cameraViewMatrix = Camera.ViewMatrix;
            Matrix projectionMatrix = D3D.ProjectionMatrix;
            Matrix orthoD3DMatrix = D3D.OrthoMatrix;

            // Render the terrain buffers.
            Terrain.Render(D3D.DeviceContext);

            // Render the model using the color shader.
            if (!DTerrainShader.Render(D3D.DeviceContext, Terrain.IndexCount, worldMatrix, cameraViewMatrix, projectionMatrix, Light.AmbientColor, Light.DiffuseColour, Light.Direction, Terrain.GrassTexture.TextureResource, Terrain.SlopeTexture.TextureResource, Terrain.RockTexture.TextureResource))
                return false;

            // Turn off the Z buffer to begin all 2D rendering.
            D3D.TurnZBufferOff();

            // Turn on the alpha blending before rendering the text.
            D3D.TurnOnAlphaBlending();

            // Render the text user interface elements.
//            if (!Text.Render(D3D.DeviceContext, worldMatrix, orthoD3DMatrix))
//                return false;

            // Turn off alpha blending after rendering the text.
            D3D.TurnOffAlphaBlending();

            // Turn the Z buffer back on now that all 2D rendering has completed.
            D3D.TurnZBufferOn();

            // Present the rendered scene to the screen.
            D3D.EndScene();

            return true;
        }
    }
}
