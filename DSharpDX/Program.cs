﻿using System;
//using SharpDXWinForm;
//using DSharpDXRastertek.Series2.TutTerr02.System;

namespace DSharpDX
{
    static class Program
    {
        /// <summary>
        /// Der Haupteinstiegspunkt für die Anwendung.
        /// </summary>
        [STAThread]
        static void Main()
        {
            DSharpDXRastertek.Series2.TutTerr02.System.DSystem.StartRenderForm("My Test of RenderForm", 800, 600, false, false);
        }
    }
}
