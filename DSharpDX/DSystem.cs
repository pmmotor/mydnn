﻿using System.Drawing;
using System.Windows.Forms;
using SharpDX.Windows;
//using TestConsole;

namespace DSharpDXRastertek.Series2.TutTerr02.System
{
    public class DSystem
    {
        // Properties
        private RenderForm RenderForm { get; set; }
        public DSystemConfiguration Configuration { get; private set; }
        public static DApplication DApplication { get; set; }

        // Constructor
        public DSystem() { }

        public static void StartRenderForm(string title, int width, int height, bool vSync, bool fullScreen = true, int testTimeSeconds = 0, float divide = 12f, float[] pos = null, float[] rot = null)
        {
            DSystem system = new DSystem();
            system.Initialize(title, width, height, vSync, fullScreen, testTimeSeconds,null,  null, divide, pos, rot);
            system.RunRenderForm();
        }
        public static void RenderFile(string title, int width, int height, string file, bool vSync, bool fullScreen = true, int testTimeSeconds = 0, float divide = 12f, float[] pos = null, float[] rot = null, bool solid = false)
        {
            DSystem system = new DSystem();
            system.Initialize(title, width, height, vSync, fullScreen, testTimeSeconds, file, null, divide, pos, rot,solid);
            system.RunRenderForm();
        }
        public static void RenderMatrix(string title, int width, int height, float[,] matrix, float divide, float[] pos=null, float[] rot=null, bool solid=false)
        {
            DSystem system = new DSystem();
            system.Initialize(title, width, height, true, false, 0, null, matrix, divide, pos, rot,solid);
            system.RunRenderForm();
        }
        // Methods
        //public static void SetCamera(int x,int y,int z,int rX, int rY, int rZ)
        //{
        //    DApplication.Zone.Camera.SetPosition(x,y,z);
        //    DApplication.Zone.Camera.SetRotation(rX,rY,rZ);
        //}
        public virtual bool Initialize(string title, int width, int height, bool vSync, bool fullScreen, int testTimeSeconds, string file=null, float[,] data=null, float scale = 12f, float[] pos=null, float[] rot=null, bool solid = false)
        {
            bool result = true;

            if (Configuration == null)
                Configuration = new DSystemConfiguration(title, width, height, fullScreen, vSync);

            // Initialize Window.
            InitializeWindows(title);

            // Create the application wrapper object.
            DApplication = new DApplication();

            // Initialize the application wrapper object.
            if (!DApplication.Initialize(Configuration, RenderForm.Handle, title, file, data, scale,pos,rot,solid))
                return false;

            return result;
        }
        private void InitializeWindows(string title)
        {
            int width = Screen.PrimaryScreen.Bounds.Width;
            int height = Screen.PrimaryScreen.Bounds.Height;

            // Initialize Window.
            RenderForm = new RenderForm(title)
            {
                ClientSize = new Size(Configuration.Width, Configuration.Height),
                FormBorderStyle = FormBorderStyle.None
            };

            // The form must be showing in order for the handle to be used in Input and Graphics objects.
            RenderForm.Show();
            RenderForm.Location = new Point((width / 2) - (Configuration.Width / 2), (height / 2) - (Configuration.Height / 2));
        }
        private void RunRenderForm()
        {
            RenderLoop.Run(RenderForm, () =>
            {
                if (!Frame())
                    ShutDown();
            });
        }
        public bool Frame()
        {
            // Read the user input.
            if (!DApplication.Input.Frame() || DApplication.Input.IsEscapePressed())
                return false;

            // Update the system stats.
            DApplication.Timer.Frame2();
            //if (DPerfLogger.IsTimedTest)
            //{
            //    DPerfLogger.Frame(DApplication.Timer.FrameTime);
            //    if (DApplication.Timer.CumulativeFrameTime >= DPerfLogger.TestTimeInSeconds * 1000)
            //        return false;
            //}

            // Do the frame processing for the application object.
            if (!DApplication.Frame())
                return false;

            return true;
        }
        public void ShutDown()
        {
            ShutdownWindows();
//            DPerfLogger.ShutDown();

            // Release the graphics object.
            DApplication?.Shutdown();
            DApplication = null;
            Configuration = null;
        }
        private void ShutdownWindows()
        {
            RenderForm?.Dispose();
            RenderForm = null;
        }
    }
}
